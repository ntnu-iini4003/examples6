#include <boost/asio.hpp>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

class Workers {
public:
  /// Provides asynchronous service
  boost::asio::io_service service;

private:
  /// See Workers-constructor. Makes service.run() wait for tasks through service.post
  boost::asio::io_service::work work;
  vector<thread> threads;

public:
  Workers(size_t number_of_threads) : work(service) {
    for (size_t c = 0; c < number_of_threads; ++c) {
      threads.emplace_back([this] {
        service.run(); // Wait for, and execute, tasks until service.stop() is called
      });
    }
  }

  /// Stop the workers
  void stop() {
    service.stop();
    // Wait for the threads to finish
    for (auto &thread : threads) {
      thread.join();
    }
  }
};

int main() {
  Workers event_loop(1);
  Workers worker_threads(5);

  cout << "Press enter to exit" << endl;

  event_loop.service.post([&event_loop, &worker_threads] {
    // Event loop starts here (think of this as the main() of the event loop)

    // Do some heavy work in a worker_thread
    cout << "Starting heavy work from a worker thread here" << endl;
    worker_threads.service.post([&event_loop] {
      // Simulate heavy work taking 2 seconds
      this_thread::sleep_for(2s);
      // When done, send result to event loop:
      auto result = make_shared<string>("Some result");
      event_loop.service.post([result] {
        // Do something with the result in the event loop
        cout << "Result from a worker thread: " << *result << endl;
      });
    });

    // Do some heavy work in another worker_thread
    cout << "Starting heavy work from another worker thread here" << endl;
    worker_threads.service.post([&event_loop] {
      // Simulate heavy work taking 1 second
      this_thread::sleep_for(1s);
      // When done, send result to event loop:
      auto result = make_shared<string>("Some another result");
      event_loop.service.post([result] {
        // Do something with the result in the event loop
        cout << "Result from another worker thread: " << *result << endl;
      });
    });
  });

  string line;
  getline(cin, line); // stop when enter is pressed
  worker_threads.stop();
  event_loop.stop();
}
// Output
// Starting heavy work from a worker thread here
// Starting heavy work from another worker thread here
// Result from another worker thread: Some another result
// Result from a worker thread: Some result
