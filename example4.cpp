#include <boost/asio.hpp>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

class Workers {
public:
  /// Provides asynchronous service
  boost::asio::io_service service;

private:
  /// See Workers-constructor. Makes service.run() wait for tasks through service.post
  boost::asio::io_service::work work;
  vector<thread> threads;

public:
  Workers(size_t number_of_threads) : work(service) {
    for (size_t c = 0; c < number_of_threads; ++c) {
      threads.emplace_back([this] {
        service.run(); // Wait for, and execute, tasks until service.stop() is called
      });
    }
  }

  /// Stop the workers
  void stop() {
    service.stop();
    // Wait for the threads to finish
    for (auto &thread : threads) {
      thread.join();
    }
  }
};

int main() {
  Workers worker(1); // Use only 1 worker (1 worker thread)

  cout << "Press enter to exit" << endl;

  std::vector<thread> threads;
  for (size_t c = 0; c < 4; ++c) {
    threads.emplace_back([&worker, c] {
      // Do some heavy work here
      this_thread::sleep_for(1s); // Simulate heavy work taking 1 second

      // Send the result to worker
      auto result = make_shared<string>("some result from thread " + to_string(c));
      worker.service.post([result] {
        // This function is guaranteed to run sequentially (1 thread)
        // because worker is running only one worker thread
        cout << "The result was: " << *result << endl;
      });
    });
  }

  string line;
  getline(cin, line); // stop when enter is pressed
  worker.stop();

  for (auto &thread : threads)
    thread.join();
}
// Mulig utskrift:
// Press enter to exit
// The result was: some result from thread 0
// The result was: some result from thread 2
// The result was: some result from thread 1
// The result was: some result from thread 3
