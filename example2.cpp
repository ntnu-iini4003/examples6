#include <iostream>
#include <memory>
#include <string>
#include <thread>

using namespace std;

// to demonstrate the problem, we need to move the below calls to another stack frame (away from main()):
void f() {
  shared_ptr<string> message(new string("some message")); // message pointer count is increased by one
  thread a_thread([message] {                             // message pointer count is increased by one
    cout << "message from a_thread: " << *message << endl;
  }); // message pointer count is decreased by one
  a_thread.detach();
} // message pointer count is decreased by one

int main() {
  f();

  cout << "from main thread" << endl;

  this_thread::sleep_for(50ms); // wait 50ms before return from main()
}
