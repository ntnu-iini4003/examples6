#include <boost/asio.hpp>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

class Workers {
public:
  /// Provides asynchronous service
  boost::asio::io_service service;

private:
  /// See Workers-constructor. Makes service.run() wait for tasks through service.post
  boost::asio::io_service::work work;
  vector<thread> threads;

public:
  Workers(size_t number_of_threads) : work(service) {
    for (size_t c = 0; c < number_of_threads; ++c) {
      threads.emplace_back([this] {
        service.run(); // Wait for, and execute, tasks until service.stop() is called
      });
    }
  }

  /// Stop the workers
  void stop() {
    service.stop();
    // Wait for the threads to finish
    for (auto &thread : threads) {
      thread.join();
    }
  }
};

int main() {
  Workers workers(4); // Use 4 workers (worker threads)

  cout << "Press enter to exit" << endl;

  // Give tasks to the workers:
  workers.service.post([&workers] {
    cout << "task A is being performed by a worker" << endl;

    // One can post new tasks inside a post:
    workers.service.post([] {
      cout << "task B is being performed by a worker" << endl;
    });
  });
  workers.service.post([] {
    cout << "task C is being performed by a worker" << endl;
  });

  string line;
  getline(cin, line); // stop when enter is pressed
  workers.stop();
}
// Mulig utskrift:
// Press enter to exit
// task A is being performed by a worker
// task C is being performed by a worker
// task B is being performed by a worker
